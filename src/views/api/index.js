import axios from "axios";
import { Message } from "element-ui";

export function commAxiosRequest(param) {
  if (param.ajaxType == "post") {
    return axios
      .post(param.apiurl, {
        params: param.apiparam.query,
        data: param.apiparam.body,
        headers:
          param.headjson == null || param.headjson == ""
            ? {}
            : JSON.parse(param.headjson),
      })
      .catch((error) => {
        Message.error("请求地址报错！" + error);
      });
  } else {
    return axios
      .get(param.apiurl, {
        params: param.apiparam.query,
        headers:
          param.headjson == null || param.headjson == ""
            ? {}
            : JSON.parse(param.headjson),
      })
      .catch((error) => {
        Message.error("请求地址报错！" + error);
      });
  }
}
